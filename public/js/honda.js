/**
 * Created by sunarcphp on 3/7/18.
 */

$(document).ready(function () {
    $(".remove-sales-user").click(function () {
        var href = $(this).data('href');
        var title = $(this).data('title');
        var delete_button = $(this).parentsUntil('tr').parent();
        if (!confirm('Do you want to delete ' + title + '?')) {
            return false;
        }
        $.ajax({
            url: href,
            type: 'GET',
            success: function (data) {
                if (data.type == 1) {
                    alert(data.message);
                    delete_button.remove();
                    location.reload();
                }
                else {
                    alert(data.message);
                }
            }
        });
    });

    $(".remove-product").click(function () {
        var href = $(this).data('href');
        var title = $(this).data('title');
        var delete_button = $(this).parentsUntil('tr').parent();
        if (!confirm('Do you want to delete ' + title + '?')) {
            return false;
        }
        $.ajax({
            url: href,
            type: 'GET',
            success: function (data) {
                if (data.type == 1) {
                    alert(data.message);
                    delete_button.remove();
                    location.reload();
                }
                else {
                    alert(data.message);
                }
            }
        });
    });
});