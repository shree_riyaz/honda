@extends('layouts.master')
@section('title')
    Add Sales Man
@endsection
@section('heading')
Add Sales Man
@endsection


@section('css')

@endsection
@section('section')

    <div class="container-fluid">
        @include('partial.breadcrumb',['levelOne'=>'Show Sales Persons','levelOneLink'=>'show-sales-man','levelTwo'=>'Add Sales Man','levelTwoLink'=>'add-sales-man'])
        <div class="row">

            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add Sales Person
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            {!! Form::open(['url' => 'add/sales-man']) !!}
                            <div class="col-lg-6">

                                    <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }} ">
                                        <label>First Name <span class="asterisk">*</span></label>
                                        <input class="form-control" type="text" value="{{ old('first_name') }}" name="first_name" placeholder="Enter first name">
                                        <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                    </div>
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                        <label>Email <span class="asterisk">*</span></label>
                                        <input class="form-control" value="{{ old('email') }}" type="email" name="email" placeholder="Enter email">
                                    <span class="text-danger">{{ $errors->first('email') }}</span>

                                </div>
                                <div class="form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">
                                    <label>Username <span class="asterisk">*</span></label>
                                    <input class="form-control" type="text" value="{{ old('user_name') }}" name="user_name" placeholder="Enter username">
                                    <span class="text-danger">{{ $errors->first('user_name') }}</span>

                                </div>

                                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                        <label>Password <span class="asterisk">*</span></label>
                                        <input class="form-control" value="{{ old('password') }}"  type="password" name="password" placeholder="Enter password">
                                    <span class="text-danger">{{ $errors->first('password') }}</span>

                                </div>
                                <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                                    <label>Address <span class="asterisk">*</span></label>
                                    <textarea class="form-control" value="" name="address" placeholder="Enter address" rows="">{{ old('address') }}</textarea>
                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                    <label>Last Name <span class="asterisk">*</span></label>
                                    <input class="form-control" name="last_name" type="text" value="{{ old('last_name') }}" placeholder="Enter last name">
                                    <span class="text-danger">{{ $errors->first('last_name') }}</span>

                                </div>
                                <div class="form-group {{ $errors->has('phone_no') ? 'has-error' : '' }}">
                                    <label>Phone Number <span class="asterisk">*</span></label>
                                    <input class="form-control" type="text" value="{{ old('phone_no') }}" name="phone_no" placeholder="Enter phone number">
                                    <span class="text-danger">{{ $errors->first('phone_no') }}</span>

                                </div>
                                <div class="form-group {{ $errors->has('user_status') ? 'has-error' : '' }}">
                                    <label>Status <span class="asterisk">*</span></label>
                                    <select name="user_status" class="form-control">
                                        <option value="" >Select user status</option>
                                        <option value="enable" {{ old('user_status') == 'enable' ? 'selected' : ''}}>Enable</option>
                                        <option value="disable" {{old('user_status') == 'disable' ? 'selected' : ''}}>Disable</option>
                                    </select>
                                    <span class="text-danger">{{ $errors->first('user_status') }}</span>

                                </div>

                                <div class="form-group {{ $errors->has('confirm_password') ? 'has-error' : '' }}">
                                    <label>Confirm Password <span class="asterisk">*</span></label>
                                    <input class="form-control" value="{{ old('confirm_password') }}" type="password" name="confirm_password" placeholder="Enter confirm password">
                                    <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                                </div>
                                <div class="form-group {{ $errors->has('date_of_joining') ? 'has-error' : '' }}">
                                    <label>Date of Joining <span class="asterisk">*</span></label>

                                    <div class="input-group date datepicker" data-provide="datepicker">
                                        <input value="{{ old('date_of_joining') }}" name="date_of_joining" type="text" class="form-control">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>

                                    <span class="text-danger">{{ $errors->first('date_of_joining') }}</span>

                                </div>
                                <button type="submit" class="btn btn-success">Save</button>
                                {{--<button type="reset" class="btn btn-default">Reset</button>--}}
                                <a  class="btn btn-danger" href="{{ url('show/sales-man') }}">Cancel</a>
                            </div>

                            {!! Form::close() !!}

                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>
@endsection
