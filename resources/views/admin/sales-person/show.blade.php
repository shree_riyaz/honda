@extends('layouts.master')
@section('title')
Sales Man List
@endsection
@section('heading')
Sales Man List
@endsection

@section('css')
@endsection
@section('section')

    <div class="container-fluid">
        @include('partial.breadcrumb',['levelOne'=>'Show Sales Person','levelOneLink'=>'show-sales-man','levelTwo'=>null,'levelTwoLink'=>null])
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Sales Man List
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover example" >
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Phone number</th>
                                <th title="Date of joining">DOJ</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($user_list as $user_lists)
                                <tr class="odd gradeX">
                                    <td>{{ ucfirst($user_lists->first_name) }} {{ ucfirst($user_lists->last_name) }}</td>
                                    <td>{{ $user_lists->email ? $user_lists->email: 'NA' }}</td>
                                    <td>{{ $user_lists->user_status ? ucfirst($user_lists->user_status) : 'NA' }}</td>
                                    <td class="center">{{ $user_lists->phone_no ? $user_lists->phone_no : 'NA' }}</td>
                                    <td class="center">{{ $user_lists->date_of_joining ? $user_lists->date_of_joining : 'NA' }}</td>
                                    <td class="action">
                                        {{--<span><a title="Edit Sales Person" class="btn btn-xs btn-primary"--}}
                                                 {{--href="{{ url('edit/sales-man').'/'.$user_lists->id }}">Edit</a>--}}


                                        {{--<a  title="Delete Sales Person" class="btn btn-xs btn-danger" onclick="if(!confirm('Are you sure want to delete this user record.')){return false;}"--}}
                                                 {{--href="{{ url('delete/sales-man').'/'.$user_lists->id }}">Delete</a></span>--}}

                                        <span>
                                            <a title="Edit Sales Person" class="btn btn-xs btn-primary" href="{{ url('edit/sales-man').'/'.$user_lists->id }}">Edit</a>
                                            <a  title="Delete Sales Person" class="btn btn-xs btn-danger remove-sales-user" data-title="{{$user_lists->first_name}}" data-href="{{ url('delete/sales-man').'/'.$user_lists->id.'?_token='.csrf_token() }}" href="#">Delete</a>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>
@endsection
@section('js')

    <script>
        $(document).ready(function () {
            $('.example').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'excel',
                    footer: true,
                    exportOptions: {
                        columns: [0,1,2,3,4,5]
                    }
                }],
            });

        });
    </script>


@endsection