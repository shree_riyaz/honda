@extends('layouts.master')
@section('title')
    Follow-up Report
@endsection
@section('heading')
    Follow-up Report
@endsection

@section('css')
    <link href="{{ url('https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css')}}" rel="stylesheet">
@endsection
@section('section')

    <div class="container-fluid">
{{--        @include('partial.breadcrumb',['levelOne'=>'Show Sales Person','levelOneLink'=>'show-sales-man','levelTwo'=>null,'levelTwoLink'=>null])--}}

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Follow-up Report
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        {!! Form::open(['url' => 'show/report','files'=>true]) !!}

                        <div class="row">
                            <div class="col-lg-3">

                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label> Search by date </label>

                                </div>
                            </div>
                            <div class="col-lg-5">

                                <div class="form-group {{ $errors->has('product_image') ? 'has-error' : '' }}">
                                    <input class="form-control" type="text" name="daterange" value="" placeholder="Select a date range" />

                                    <span class="text-danger">{{ $errors->first('name') }}</span>

                                </div>
                            </div>
                            <div class="col-lg-4">

                                <button type="submit" class="btn btn-default ">Show</button>
                                <a class="btn btn-danger ml-4" href="{{ URL::to('show/report') }}">Reset</a>
                                {{--<button type="submit" class="btn btn-default ">Reset</button>--}}
                            </div>

                        </div>

                        {!! Form::close() !!}

                        <hr>

                        <div class="row">
                            <div class="col-lg-12">
                                <table width="100%" class="table table-striped table-bordered table-hover example" id="">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Sales person</th>
                                        <th>Product</th>
                                        <th>Detail</th>
                                        <th title="Date of communication">Follow-up Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($enquiry_list as $enquiry_lists)
                                        <tr class="odd gradeX">
                                            <td>{{ ucfirst($enquiry_lists->first_name) }} {{ ucfirst($enquiry_lists->last_name) }}</td>
                                            <td>{{ $enquiry_lists->email ? $enquiry_lists->email: 'N/A' }}</td>
                                            <td>{{ $enquiry_lists->phone_no ? $enquiry_lists->phone_no : 'N/A' }}</td>
                                            <td class="center">{{ $enquiry_lists->salesman->first_name }} {{ $enquiry_lists->salesman->last_name }}</td>
                                            <td class="center">{{ $enquiry_lists->products->name ? $enquiry_lists->products->name : 'N/A' }} </td>
                                            <td class="center">{{ $enquiry_lists->enquiry_description ? $enquiry_lists->enquiry_description : 'N/A' }} </td>
                                            <td class="center">{{ $enquiry_lists->created_at ? date('d-m-Y',strtotime($enquiry_lists->created_at)) : 'N/A' }}</td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

    </div>
@endsection
@section('js')
    <script src="{{ url('https://cdn.jsdelivr.net/momentjs/latest/moment.min.js')}}" ></script>
    <script src="{{ url('https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js')}}" ></script>
    <script>
        // $(document).ready(function() {
        //     $('#dataTables-example').DataTable({
        //         responsive: true
        //     });
        // });
        $(document).ready(function () {
            $('.example').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'excel',
                    footer: true,
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6]
                    }
                }],
            });

        });
    </script>

    <script>
        $(function() {
            $('input[name="daterange"]').daterangepicker({
                opens: 'left',
                maxDate: moment().endOf("day"),
                autoUpdateInput: false,
                locale: {
                    format: 'DD-MM-YYYY'
                }

            }, function(start_date, end_date) {
                $('input[name="daterange"]').val(start_date.format('DD-MM-YYYY')+' - '+end_date.format('DD-MM-YYYY'));
            });
        });
    </script>
    {{--http://www.daterangepicker.com--}}
@endsection