@extends('layouts.master')
@section('title')
    Update Admin Profile
@endsection
@section('heading')
    Update Admin Profile
@endsection
@section('section')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Update Admin Profile
                    </div>
                    <div class="panel-body">
                        <div class="row">
                                {!! Form::open(['url' => 'update-profile']) !!}
                            <div class="col-lg-6">

                                <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                    <label>First Name <span class="asterisk">*</span></label>
                                    <input class="form-control" type="text" value="{{ $user_detail->first_name ? $user_detail->first_name : 'NA' }}" name="first_name" placeholder="Enter first name">
                                    <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                </div>
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <label>Email</label>
                                    <input readonly class="form-control" value="{{ $user_detail->email ? $user_detail->email : 'NA' }}" type="email" name="email" placeholder="Enter email">
                                    <span class="text-danger">{{ $errors->first('email') }}</span>

                                </div>
                                <div class="form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">
                                    <label>Username <span class="asterisk">*</span></label>
                                    <input class="form-control" type="text" value="{{ $user_detail->user_name ? $user_detail->user_name : 'NA' }}" name="user_name" placeholder="Enter username">
                                    <span class="text-danger">{{ $errors->first('user_name') }}</span>

                                </div>


                            </div>
                            <div class="col-lg-6">
                                <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                    <label>Last Name <span class="asterisk">*</span></label>
                                    <input class="form-control" name="last_name" type="text" value="{{ $user_detail->last_name ? $user_detail->last_name : 'NA' }}" placeholder="Enter last name">
                                    <span class="text-danger">{{ $errors->first('last_name') }}</span>

                                </div>
                                <div class="form-group {{ $errors->has('phone_no') ? 'has-error' : '' }}">
                                    <label>Phone Number <span class="asterisk">*</span></label>
                                    <input class="form-control" type="text" value="{{ $user_detail->phone_no ? $user_detail->phone_no : date('Y-m-d') }}" name="phone_no" placeholder="Enter phone number">
                                    <span class="text-danger">{{ $errors->first('phone_no') }}</span>

                                </div>
                                <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                                    <label>Address <span class="asterisk">*</span></label>
                                    <textarea class="form-control" value="" name="address" placeholder="Enter address" rows="">{{ $user_detail->address ? $user_detail->address : 'NA' }}</textarea>
                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                </div>
                                {{--<div class="form-group {{ $errors->has('user_status') ? 'has-error' : '' }}">--}}
                                    {{--<label>Status</label>--}}
                                    {{--<select name="user_status" class="form-control">--}}
                                        {{--<option ></option>--}}
                                        {{--<option value="enable" {{ $user_detail->user_status == 'enable' ? 'selected' : '' }} >Enable</option>--}}
                                        {{--<option value="disable" {{ $user_detail->user_status == 'disable' ? 'selected' : '' }}>Disable</option>--}}
                                    {{--</select>--}}
                                    {{--<span class="text-danger">{{ $errors->first('user_status') }}</span>--}}

                                {{--</div>--}}

                                {{--<div class="form-group {{ $errors->has('date_of_joining') ? 'has-error' : '' }}">--}}
                                    {{--<label>Date of Joining</label>--}}

                                    {{--<div class="input-group date datepicker" data-provide="datepicker">--}}
                                        {{--<input value="{{ old('date_of_joining') }}" name="date_of_joining" type="text" class="form-control">--}}
                                        {{--<div class="input-group-addon">--}}
                                            {{--<span class="glyphicon glyphicon-th"></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<span class="text-danger">{{ $errors->first('date_of_joining') }}</span>--}}

                                {{--</div>--}}
                                <input type="hidden" value="{{ $user_detail->id }}" name="admin_id">
                                <button type="submit" class="btn btn-success">Update</button>
                                <a  class="btn btn-danger" href="{{ URL::to('admin') }}">Cancel</a>
                                {{--<button type="reset" class="btn btn-default">Reset</button>--}}
                            </div>

                            {!! Form::close() !!}

                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>
@endsection
