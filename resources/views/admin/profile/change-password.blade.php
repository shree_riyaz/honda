@extends('layouts.master')
@section('title')
   Change Password
@endsection
@section('heading')
    Change Password
@endsection
@section('section')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Change Password
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            {!! Form::open(['url' => 'change-password']) !!}

                            <div class="col-lg-4">

                                <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                                    <label>Current Password <span class="asterisk">*</span></label>
                                    <input class="form-control" value="{{ old('password') }}"  type="password" name="current_password" placeholder="Enter old password">
                                    <span class="text-danger">{{ $errors->first('current_password') }}</span>

                                </div>

                            </div>
                                <div class="col-lg-4">

                                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                    <label>Password <span class="asterisk">*</span></label>
                                    <input class="form-control" value="{{ old('password') }}"  type="password" name="password" placeholder="Enter password">
                                    <span class="text-danger">{{ $errors->first('password') }}</span>

                                </div>

                            </div>
                            <div class="col-lg-4">


                                <div class="form-group {{ $errors->has('confirm_password') ? 'has-error' : '' }}">
                                    <label>Confirm Password <span class="asterisk">*</span></label>
                                    <input class="form-control" type="password" name="confirm_password" placeholder="Enter confirm password">
                                    <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                                </div>


                                <input type="hidden" value="{{ $user_detail->id }}" name="admin_id">
                                <button type="submit" class="btn btn-success">Update</button>
                                {{--<button type="reset" class="btn btn-default">Reset</button>--}}
                                <a  class="btn btn-danger" href="{{ URL::to('admin') }}">Cancel</a>
                            </div>

                            {!! Form::close() !!}

                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>
@endsection
