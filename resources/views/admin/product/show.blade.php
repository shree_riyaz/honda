@extends('layouts.master')
@section('title')
    Product Lists
@endsection
@section('heading')
    Product Lists
@endsection

@section('css')
    {{--<link href="{{ url('vendor/datatables-plugins/dataTables.bootstrap.css')}}" rel="stylesheet">--}}

@endsection
@section('section')
    <div class="container-fluid">
        @include('partial.breadcrumb',['levelOne'=>'Show Products','levelOneLink'=>'show-products','levelTwo'=>null,'levelTwoLink'=>null])

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Product Lists
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        {{--<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">--}}
                        <table width="100%" class="table table-striped table-bordered table-hover example" id="">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($product_list as $product_lists)

                                <tr class="odd gradeX">
                                    <td>{{ $product_lists->name ? $product_lists->name: 'NA' }}</td>
                                    <td>{{ ($product_lists->status == 'a') ? 'Active' : 'Inactive' }}</td>
                                    <td class="center">
                                        <img class="img-thumbnail" src="{{ url('product/images/'.$product_lists->product_image ) }}" alt="Product {{ $product_lists->product_image }}" height="100" width="100">

                                    </td>
                                    <td class="action">

                                        <span><a title="Edit Sales Person" class="btn btn-xs btn-primary"
                                                 href="{{ url('edit/product').'/'.$product_lists->id }}">Edit</a>


<a  title="Delete Product" class="btn btn-xs btn-danger remove-product" data-title="{{$product_lists->first_name}}" data-href="{{ url('delete/product').'/'.$product_lists->id.'?_token='.csrf_token() }}" href="#">Delete</a></span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>
@endsection
@section('js')

    <script>
        $(document).ready(function () {
            $('.example').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'excel',
                    footer: true,
                    exportOptions: {
                        columns: [0,1]
                    }
                }],
            });

        });
    </script>

@endsection