@extends('layouts.master')
@section('title')
    Add Product
@endsection
@section('heading')
    Add Product
@endsection

@section('css')
    <link href="{{ url('css/datepicker.css')}}" rel="stylesheet">

@endsection
@section('section')
    <div class="container-fluid">
        @include('partial.breadcrumb',['levelOne'=>'Show Products','levelOneLink'=>'show-products','levelTwo'=>'Add Product','levelTwoLink'=>'add-product'])
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add Product
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            {!! Form::open(['url' => 'add/product','files'=>true]) !!}
                            <div class="col-lg-6">

                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                        <label>Name <span class="asterisk">*</span></label>
                                        <input class="form-control" type="text" value="{{ old('name') }}" name="name" placeholder="Enter name">
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>


                                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                                    <label>Description <span class="asterisk">*</span></label>
                                    <textarea class="form-control" value="" name="description" placeholder="Enter Description" rows="">{{ old('description') }}</textarea>
                                    <span class="text-danger">{{ $errors->first('description') }}</span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                    <label>Status <span class="asterisk">*</span></label>
                                    <select name="status" class="form-control">
                                        <option value="">Select status</option>
                                        <option {{old('status') == 'a' ? 'selected' : ''}} value="a">Active</option>
                                        <option {{old('status') == 'i' ? 'selected' : ''}} value="i">Inactive</option>
                                    </select>
                                    <span class="text-danger">{{ $errors->first('status') }}</span>

                                </div>
                                <div class="form-group {{ $errors->has('product_image') ? 'has-error' : '' }}">
                                    <label>Image <span class="asterisk">*</span></label>
                                    <input class="form-control" type="file" name="product_image">
                                    <span class="text-danger">{{ $errors->first('product_image') }}</span>

                                </div>

                                <button type="submit" class="btn btn-success">Save</button>
                                <a href="{{ url('show/products') }}" class="btn btn-danger">Cancel</a>
                                {{--<button type="reset" class="btn btn-default">Reset</button>--}}
                            </div>

                            {!! Form::close() !!}

                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ url('js/bootstrap-datepicker.js')}}"></script>

    <script>
    </script>
    @endsection