@extends('layouts.master')
@section('title')
    Edit Product
@endsection
@section('heading')
    Edit Product
@endsection

@section('css')
    <link href="{{ url('css/datepicker.css')}}" rel="stylesheet">

@endsection
@section('section')
    <div class="container-fluid">
        @include('partial.breadcrumb',['levelOne'=>'Show Sales Persons','levelOneLink'=>'show-sales-man','levelTwo'=>'Edit','levelTwoLink'=>null])

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit Product
                    </div>
                    <div class="panel-body">
                        {!! Form::open(['url' => 'edit/product','files'=>true]) !!}

                        <div class="row">
                            <div class="col-lg-6">

                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label>Name <span class="asterisk">*</span></label>
                                    <input class="form-control" type="text" value="{{ $product_detail->name }}" name="name" placeholder="Enter first name">
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                </div>


                                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                                    <label>Description <span class="asterisk">*</span></label>
                                    <textarea class="form-control" name="description" placeholder="Enter Description" rows="">{{ $product_detail->description }}</textarea>
                                    <span class="text-danger">{{ $errors->first('description') }}</span>
                                </div>

                                <div class="col-md-4">
                                    <img class="img-thumbnail" src="{{ url('product/images/'.$product_detail->product_image ) }}" alt="Product {{ $product_detail->product_image }}" height="200" width="200">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                    <label>Status <span class="asterisk">*</span></label>
                                    <select name="status" class="form-control">
                                        <option value="">Select status</option>
                                        <option {{$product_detail->status == 'a' ? 'selected' : ''}} value="a">Active</option>
                                        <option {{$product_detail->status == 'i' ? 'selected' : ''}} value="i">Inactive</option>
                                    </select>
                                    <span class="text-danger">{{ $errors->first('status') }}</span>

                                </div>
                                <div class="form-group {{ $errors->has('product_image') ? 'has-error' : '' }}">
                                    <div class="col-md-8">
                                        <label>Image <span class="asterisk">*</span></label>
                                        <input class="form-control" value="{{ $product_detail->product_image }}" type="file" name="product_image">
                                        <span class="text-danger">{{ $errors->first('product_image') }}</span>
                                    </div>


                                    <input type="hidden" name="product_id" value="{{ $product_detail->id }}">

                                </div>

                            </div>



                        </div>
                        <div class="row">
                            <div class="col-md-9 margin-top-50">

                                <a href="{{ url('show/products') }}" style="margin-left: 5px;" class="btn btn-danger pull-right">Cancel</a>
                                <button type="submit" class="btn btn-success pull-right ">Update</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ url('js/bootstrap-datepicker.js')}}"></script>

    <script>
    </script>
@endsection