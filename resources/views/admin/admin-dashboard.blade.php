@extends('layouts.master')
@section('title')
    Admin Dashboard
@endsection
@section('heading')
    Admin dashboard
@endsection
@section('section')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Admin dashboard
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <a href="{{ url('show/sales-man') }}">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-users fa-4x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge">{{ $total_sales_person }}</div>
                                                <div>Total Sales Persons</div>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="panel-footer">
                                            <span class="pull-left">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>

                                </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <a href="{{ url('show/products') }}">
                                <div class="panel panel-green">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-shopping-cart fa-4x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge">{{ $total_products }}</div>
                                                <div>Total Products</div>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="panel-footer">
                                            <span class="pull-left">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>

                                </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <a href="{{ url('show/report') }}">
                                <div class="panel panel-yellow">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-microphone fa-4x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge">{{ $totalInquiry }}</div>
                                                <div>Total Follow Up</div>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="panel-footer">
                                            <span class="pull-left">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>

                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
