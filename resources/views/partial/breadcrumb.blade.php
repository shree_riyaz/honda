{{--<div class="am-cf am-padding btn-group btn-breadcrumb">--}}
    {{--<div class="am-fl am-cf "><a class="btn btn-default" href="{{ url('admin') }}"><i class="glyphicon glyphicon-home"></i></a>--}}
        {{--@if($levelOne != null)--}}
             {{--@if($levelOneLink != null)--}}
                {{--<a class="btn btn-default" href="{{ route($levelOneLink) }}">{{$levelOne}}</a>--}}
            {{--@else--}}
                {{--<strong>  {{$levelOne}} </strong>--}}
            {{--@endif--}}
        {{--@endif--}}
        {{--@if($levelTwo != null)--}}
             {{--@if($levelTwoLink != null)--}}
                {{--<a class="btn btn-default" href="{{ route($levelTwoLink) }}">{{$levelTwo}}</a>--}}
            {{--@else--}}
                {{--<strong>  {{$levelTwo}} </strong>--}}
            {{--@endif--}}
        {{--@endif--}}
    {{--</div>--}}
{{--</div>--}}

<div class="row">
    <div class="btn-group btn-breadcrumb breadcrump-global">
        <a href="#" class="btn btn-default"><i class="glyphicon glyphicon-home"></i></a>
        @if( $levelOne != null)
            @if($levelOneLink == null)
                <a href="#" class="btn btn-default">{{$levelTwo}}</a>
            @elseif($levelOneLink != null)
                <a href="{{ route($levelOneLink) }}" class="btn btn-default">{{$levelOne}}</a>
            @else
                <strong>  {{$levelOne}} </strong>
            @endif
        @endif
        @if($levelTwo != null)
            @if($levelTwoLink == null)
                <a href="#" class="btn btn-default">{{$levelTwo}}</a>
            @elseif($levelOneLink != null)
                <a href="{{ route($levelTwoLink) }}" class="btn btn-default">{{$levelTwo}}</a>

            @else
                <strong>  {{$levelTwo}} </strong>
            @endif
        @endif
    </div>
</div>

{{--https://bootsnipp.com/snippets/featured/triangle-breadcrumbs-arrows--}}