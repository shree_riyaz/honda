<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}

<!-- Bootstrap Core CSS -->
    <link href=" {{ url('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href=" {{ url('vendor/metisMenu/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ url('css/sb-admin-2.min.css')}}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ url('vendor/morrisjs/morris.css')}}" rel="stylesheet">
    <link href="{{ url('css/style.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ url('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ url('css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    {{--<link href="{{ url('vendor/datatables-plugins/dataTables.bootstrap.css')}}" rel="stylesheet">--}}
    {{--<link href="{{ url('vendor/datatables-responsive/dataTables.responsive.css')}}" rel="stylesheet">--}}
    <link href="{{url('panel/data-table/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{url('panel/data-table/css/buttons.dataTables.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
@yield('css')
    <!-- Fonts -->
    {{--<link rel="dns-prefetch" href="https://fonts.gstatic.com">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">--}}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>


    <![endif]-->

    <!-- Styles -->
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('admin')}}">Honda Enquiry Management </a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
            @else
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ url('update-profile') }}"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="{{ url('change-password') }}"><i class="fa fa-key fa-fw"></i> Change Password</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i> {{ __('Logout') }}</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
                @endguest
        </ul>
        <!-- /.navbar-top-links -->


        <div class="navbar-default sidebar" role="navigation">

            @guest
            @else
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="{{url('admin')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Sales Person<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse" aria-expanded="true" style="">
                                <li>
                                    <a href="{{ url('add/sales-man') }}">Add Sales Person</a>
                                </li>
                                <li>
                                    <a href="{{ url('show/sales-man') }}">Show Sales Person </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-briefcase fa-fw"></i> Product <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse" aria-expanded="true" style="">
                                <li>
                                    <a href="{{ url('add/product') }}">Add Product</a>
                                </li>
                                <li>
                                    <a href="{{ url('show/products') }}">Show Products</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Report <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse" aria-expanded="true" style="">
                                <li>
                                    <a href="{{ url('show/report') }}">Follow-up</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            @endguest
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        @include('flash::message')
       {{-- <div class="row">
            <div class="col-lg-12">
                <div class="btn-group btn-breadcrumb">
                    <a href="{{ url('admin') }}" class="btn btn-default"><i class="glyphicon glyphicon-home"></i></a>
                    <a href="@yield('url-href')" class="btn btn-default">@yield('url-name')</a>
                    <a href="{{URL::current()}}" class="btn btn-default">@yield('url-name')</a>
                    <a href="#" class="btn btn-default">Default</a>
                </div>
            </div>
        </div>--}}



        <div class="container-fluid">

            <div class="row">

                <div class="col-lg-12">

                    <h1 class="page-header">@yield('heading')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        @yield('section')
        <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
</div>
<!-- jQuery -->
<script src="{{ url('vendor/jquery/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ url('vendor/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{ url('vendor/metisMenu/metisMenu.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
{{--<script src="{{ url('vendor/raphael/raphael.min.js')}}"></script>--}}
{{--<script src="{{ url('vendor/morrisjs/morris.min.js')}}"></script>--}}
{{--<script src="{{ url('js/morris-data.js')}}"></script>--}}
<script src="{{ url('js/honda.js')}}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{ url('js/sb-admin-2.min.js')}}" ></script>
<script src="{{ url('js/bootstrap-datepicker.min.js')}}"></script>
{{--<script src="{{ url('vendor/datatables/js/jquery.dataTables.min.js')}}" ></script>--}}
{{--<script src="{{ url('vendor/datatables-plugins/dataTables.bootstrap.min.js')}}" ></script>--}}
{{--<script src="{{ url('vendor/datatables-responsive/dataTables.responsive.js')}}" ></script>--}}
<script>
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
         //startDate: '-3d',
        endDate: "today",
        'datesDisabled':false
    });
</script>


<script src="{{url('panel/data-table/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{url('panel/data-table/js/dataTables.buttons.min.js')}}" type="text/javascript"></script>
<script src="{{url('panel/data-table/js/buttons.flash.min.js')}}" type="text/javascript"></script>
<script src="{{url('panel/data-table/js/jszip.min.js')}}" type="text/javascript"></script>
<script src="{{url('panel/data-table/js/pdfmake.min.js')}}" type="text/javascript"></script>
<script src="{{url('panel/data-table/js/vfs_fonts.js')}}" type="text/javascript"></script>
<script src="{{url('panel/data-table/js/buttons.html5.min.js')}}" type="text/javascript"></script>
<script src="{{url('panel/data-table/js/buttons.print.min.js')}}" type="text/javascript"></script>
<script>
            @if(Session::has('message'))
                var type = "{{ Session::get('alert-type', 'info') }}";
                switch(type){
                    case 'info':
                        toastr.info("{{ Session::get('message') }}");
                        break;

                    case 'warning':
                        toastr.warning("{{ Session::get('message') }}");
                        break;

                    case 'success':
                        toastr.success("{{ Session::get('message') }}");
                        break;

                    case 'error':
                        toastr.error("{{ Session::get('message') }}");
                        break;
                }
                @endif
</script>
@yield('js')

</body>
</html>
