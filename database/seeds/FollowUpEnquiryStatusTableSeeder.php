<?php

use Illuminate\Database\Seeder;
use App\FollowUpEnquiryStatus;
class FollowUpEnquiryStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FollowUpEnquiryStatus::create([
            'status_name' => 'WARM',
            'is_active' => '1',
        ]);
        FollowUpEnquiryStatus::create([
            'status_name' => 'HOT',
            'is_active' => '1',
        ]);
        FollowUpEnquiryStatus::create([
            'status_name' => 'COLD',
            'is_active' => '1',
        ]);
    }
}
