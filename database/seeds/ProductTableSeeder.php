<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Product::create([
            'name' => 'Splendor',
            'status' => 'a',
            'description' => 'Alloy wheels Splendor',
            'product_image' => 'no_image.png',

        ]);

        \App\Product::create([
            'name' => 'Apache',
            'status' => 'i',
            'description' => 'Alloy wheels Apache',
            'product_image' => 'no_image.png',
        ]);

        \App\Product::create([
            'name' => 'Pulsar',
            'status' => 'a',
            'description' => 'Alloy wheels Pulsar',
            'product_image' => 'no_image.png',
        ]);
    }
}
