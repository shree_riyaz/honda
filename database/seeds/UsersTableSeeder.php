<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Admin',
            'last_name' => 'Kumar',
            'phone_no' => '9929594294',
            'user_name' => 'admin007',
            'user_status' => 'enable',
            'user_type' => 'admin',
            'date_of_joining' => '2012-02-05',
            'email' => 'john@mailinator.com',
            'password' => bcrypt(12345678),

        ]);

        User::create([
            'first_name' => 'David',
            'last_name' => 'Richards',
            'phone_no' => '9929594295',
            'user_status' => 'enable',
            'user_name' => 'sales007',
            'date_of_joining' => '2012-02-05',
            'user_type' => 'sales_person',
            'email' => 'sales_person@mailinator.com',
            'password' => bcrypt(12345678),

        ]);
    }
}
