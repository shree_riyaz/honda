<?php

use Illuminate\Database\Seeder;
use App\FollowUp;
class FollowupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FollowUp::create([
            'first_name' => 'Martin',
            'last_name' => 'Guptill',
            'phone_no' => '9929594295',
            'email' => 'martinwilson@mailinator.com',
            'date_of_communication' => date('Y-m-d H:i:s'),
            'address' => 'Street 12',
            'customer_feedback' => 'Good one',
            'sales_person_id' => '2',
            'product_id' => '1',
            'enquiry_description' => 'Should be improve',
            'customer_response' => 'yes',
            'is_product_sold' => 'no',
            'followup_enquiry_status' => '1',

        ]);

        FollowUp::create([
            'first_name' => 'Collin',
            'last_name' => 'Munro',
            'phone_no' => '9927494294',
            'email' => 'munro@mailinator.com',
            'date_of_communication' => date('Y-04-d H:i:s'),
            'address' => 'Street 12',
            'customer_feedback' => 'bad one',
            'sales_person_id' => '2',
            'product_id' => '1',
            'enquiry_description' => 'Should be improve',
            'customer_response' => 'yes',
            'is_product_sold' => 'no',
            'followup_enquiry_status' => '1',

        ]);
    }
}
