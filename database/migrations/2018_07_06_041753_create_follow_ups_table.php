<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowUpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('follow_ups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('phone_no',10)->nullable();
            $table->text('address')->nullable();
            $table->text('customer_feedback')->nullable();
            $table->string('sales_person_id')->nullable();
            $table->integer('user_count')->nullable();
            $table->string('product_id')->nullable();
            $table->text('enquiry_description')->nullable();
            $table->enum('customer_response', ['yes','no','later'])->nullable();
            $table->enum('is_product_sold', ['yes','no'])->nullable();
            $table->dateTime('date_of_communication')->nullable();
            $table->string('followup_enquiry_status')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('follow_ups');
    }
}
