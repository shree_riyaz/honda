<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>['guest']],function () {

    Route::get('/', function () {
    //    return view('home');
        return view('auth.login');
    });
});

Auth::routes();
Route::group(['middleware'=>['auth','admin']],function () {
//Route::get('/home', 'HomeController@index')->name('home');
    Route::get('admin', 'AdminController@dashboard')->name('admin');
    Route::get('add/sales-man', 'SalesManController@addSalesMan')->name('add-sales-man');
    Route::post('add/sales-man', 'SalesManController@postAddSalesMan')->name('add-sales-man');
    Route::get('show/sales-man', 'SalesManController@showSalesMan')->name('show-sales-man');
    Route::get('edit/sales-man/{id}', 'SalesManController@editSalesMan')->name('get-edit-sales-man');
    Route::post('edit/sales-man', 'SalesManController@updateSalesMan')->name('edit-post-sales-man');
    Route::get('delete/sales-man/{id}', 'SalesManController@deleteSalesMan')->name('delete-sales-man');

    Route::get('add/product', 'ProductController@AddProduct')->name('add-product');
    Route::post('add/product', 'ProductController@postAddProduct')->name('add-product');
    Route::get('show/products', 'ProductController@showProduct')->name('show-products');
    Route::get('edit/product/{id}', 'ProductController@editProduct')->name('edit-product');
    Route::post('edit/product', 'ProductController@updateProduct')->name('edit-product');
    Route::get('delete/product/{id}', 'ProductController@deleteProduct')->name('delete-product');

    Route::get('update-profile', 'AdminController@updateProfile')->name('update-profile');
    Route::post('update-profile', 'AdminController@postUpdateProfile')->name('update-profile');
    Route::get('change-password', 'AdminController@changePassword')->name('change-password');
    Route::post('change-password', 'AdminController@postChangePassword')->name('change-password');

    Route::any('show/report', 'ReportController@showProduct')->name('show-products');
    Route::get('filter-report', 'ReportController@showProduct')->name('show-products');


    Route::get('logout', 'Auth\LoginController@logout');

});