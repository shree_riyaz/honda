<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\UserController@login');  //done
Route::get('getservertime', 'Api\UserController@getServerTime'); //done


Route::middleware('auth:api')->group( function () {

    Route::get('products', 'Api\ProductController@showProducts')->name('show-product');
    Route::get('users', 'Api\UserController@showUsersList')->name('show-users'); //done
    Route::post('add/followup', 'Api\FollowUpController@AddFollowUp')->name('add-folloup');
    Route::post('edit/followup', 'Api\FollowUpController@updateFollowUp')->name('update-followup');

    Route::post('delete/followup', 'Api\FollowUpController@deleteFollowUp')->name('delete-followup');
    Route::get('list/followup', 'Api\FollowUpController@listFollowUp')->name('list-followup');
    Route::get('list/followup/enquiry-status', 'Api\FollowUpController@listFollowUpEnquiryStatus')->name('list-followup-status');
    Route::post('search/followup', 'Api\FollowUpController@searchFollowUp')->name('search-followup');
    Route::post('logout', 'Api\UserController@postLogout')->name('logout-user');

    // Route::resource('products', 'API\ProductController');

});

