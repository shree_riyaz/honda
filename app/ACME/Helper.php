<?php

namespace App\ACME;

use App\SalesMan;
use App\User;
use Illuminate\Support\Facades\Auth;

class Helper
{

    /**
     * @param $id
     * @return mixed
     */
    public static function getFieldNameFromFollowUpsTable($id){

        $fieldName = User::where('id',$id)->value('first_name','last_name');
        return $fieldName;
    }
}