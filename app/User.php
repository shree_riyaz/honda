<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password', 'user_type', 'user_status', 'phone_no', 'user_name', 'address', 'date_of_joining',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $dates = ['deleted_at'];

//    public function setPasswordAttribute($pass){
//
//        $this->attributes['password'] = bcrypt($pass);
//    }

    public function AauthAcessToken(){
        return $this->hasMany('\App\OauthAccessToken');
    }

}
