<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FollowupFeedback extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'customer_feedback','sales_person_id','product_id','followup_id','enquiry_description','customer_response','is_product_sold','followup_enquiry_status','attempt_number'
    ];
    protected $dates = ['deleted_at'];
}
