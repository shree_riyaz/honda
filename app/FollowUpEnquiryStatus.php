<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FollowUpEnquiryStatus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['status_name','is_active'];

    protected $table = 'followup_enquiry_status';

}
