<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FollowUp extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'first_name','last_name', 'email', 'user_count', 'address', 'customer_feedback', 'sales_person_id', 'phone_no', 'product_id', 'enquiry_description', 'customer_response','is_product_sold', 'date_of_communication','followup_enquiry_status'
    ];

    protected $dates = ['deleted_at'];

    public function salesman()
    {
        return $this->hasOne('App\User','id','sales_person_id');
    }
    public function products()
    {
        return $this->hasMany('App\Product','id','product_id');
    }
    public function followup_feedback()
    {
        return $this->hasMany('App\FollowupFeedback','followup_id','id');
    }
}
