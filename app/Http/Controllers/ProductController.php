<?php

namespace App\Http\Controllers;

use App\FollowUp;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{
    public function AddProduct(){

        return view('admin.product.add');
    }

    public function postAddProduct(Request $request){

        $this->validate($request,[
            'name' => 'required|max:90',
            'status' => 'required',
            'product_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required',
        ]);

        $image = $request->file('product_image');
        $input['imagename'] = time().'_'.$image->getClientOriginalName();
        $destinationPath = public_path('/product/images');
        $image->move($destinationPath, $input['imagename']);

        Product::create([
            'name' => $request->name,
            'product_image' => $input['imagename'],
            'status' => $request->status,
            'description' => $request->description,
        ]);
        $notification = array(
            'message' => 'Product added successfully.',
            'alert-type' => 'success'
        );

        return Redirect::to('show/products')->with($notification);

       // flash('Product added successfully.')->success();
//        dd($request->all());

      //  return redirect('show/products');
    }

    public function showProduct(){

        $product_list = Product::orderBy('id','desc')->get();
        return view('admin.product.show',compact('product_list'));
    }
    public function editProduct($id){

        $product_detail = Product::find($id);

        return view('admin.product.edit',compact('product_detail'));
    }
    public function updateProduct(Request $request){

//        dd($request->status);
        $id = $request->product_id;
        $is_image = Product::where('id',$id)->value('product_image');
        if($is_image != ''){
            $image_validation = '';
        }else{
            $image_validation = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';

        }
        $this->validate($request,[
            'name' => 'required|max:90',
            'status' => 'required',
            'product_image' => $image_validation,
            'description' => 'required',
        ]);

        $image = $request->file('product_image');

        if(isset($image)) {

        $input['imagename'] = time().'_'.$image->getClientOriginalName();
        $destinationPath = public_path('/product/images');
        $image->move($destinationPath, $input['imagename']);

        Product::where('id',$id)->update([

            'name' => $request->name,
            'status' => $request->status,
            'product_image' => $input['imagename'],
            'description' => $request->description,
        ]);
        }else{
            Product::where('id',$id)->update([

                'name' => $request->name,
                'status' => $request->status,
                'description' => $request->description,
            ]);
        }
        $notification = array(
            'message' => 'Product updated successfully.',
            'alert-type' => 'success'
        );

        return Redirect::to('show/products')->with($notification);
       // flash('Product updated successfully.')->success();
        //return redirect()->back();
    }

    public function deleteProduct(Request $request,$id){

        $finduser = Product::find($id)->delete();

        $result =  $finduser ?   1 :  0;

        $alert = [
            'type' => $result ? 1 : 0,
            'message' => $result ? 'Product deleted.' : 'Deletion failed.',
            'trId' => $id,
        ];
        return response()->json($alert);

    }


    }
