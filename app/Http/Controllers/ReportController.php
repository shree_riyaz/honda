<?php

namespace App\Http\Controllers;

use App\FollowUp;
use Illuminate\Http\Request;
use DB;

class ReportController extends Controller
{
    public function showProduct(Request $request){

        if (isset($request->daterange)){
            $explode_date = explode(' - ',$request->daterange);
            $from_date = date('Y-m-d',strtotime($explode_date[0]));
            $to_date = date('Y-m-d',strtotime($explode_date[1]));
            $enquiry_list =  FollowUp::with('salesman','products')->where(['is_product_sold'=>'no'])->whereBetween(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d")'),[$from_date, $to_date])->get();
           // echo "<pre>";print_r($enquiry_list);exit;
        }else{
            $enquiry_list =  FollowUp::with('salesman','products')->get();

        }
      //  echo "<pre>";print_r($enquiry_list);exit;
       return view('admin.report.show',compact('enquiry_list'));
    }
}
