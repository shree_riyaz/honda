<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('login');
    }
//    public function login(Request $request)
//    {
//        // set the remember me cookie if the user check the box
//        $remember = ($request->has('remember')) ? true : false;
////        dd($remember);
//        // attempt to do the login
//        $this->validate($request,[
//
//            'email' => 'required|email',
//            'password' => 'required|min:6|max:20',
//        ]);
////    dd($request->all());
//        $auth = Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember);
//        if ($auth) {
//            $notification = array(
//                'message' => 'Login successfully.',
//                'alert-type' => 'success'
//            );
//            return Redirect::to('/admin')->with($notification);
//        } else {
//            // validation not successful, send back to form
//
//            $notification = array(
//                'message' => 'Enter valid Email/Password.',
//                'alert-type' => 'success'
//            );
////            return Redirect::to('/')->with($notification);
//            return Redirect::to('/')
//                ->withInput(Input::except('password'))
//                ->with('flash_notice', 'Your username/password combination was incorrect.');
//        }
//    }
}
