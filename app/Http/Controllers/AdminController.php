<?php

namespace App\Http\Controllers;

use App\FollowUp;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    public function dashboard(){

        $total_sales_person = User::where('user_type','sales_person')->count();
        $total_products = Product::all()->count();
        $totalInquiry = FollowUp::where(['is_product_sold'=>'no'])->get()->count();
        return view('admin.admin-dashboard',compact('total_sales_person','total_products','totalInquiry'));
    }

    public function updateProfile(){

        $user_detail = $this->adminInfo();

        return view('admin.profile.profile',compact('user_detail'));
    }
    public function postUpdateProfile(Request $request){

        $user_id = $request->admin_id;
        $this->validate($request,[
            'first_name' => 'required|max:50|regex:/^[a-zA-Z]+$/u|string',
            'last_name' => 'required|max:50|regex:/^[a-zA-Z]+$/u|string',
           // 'user_status' => 'required',
            //'date_of_joining' => 'required|date|date_format:Y-m-d',
            'address' => 'required',
            'user_name' => 'required|max:50|unique:users,user_name,'.$user_id,
            'email' => 'required|email|unique:users,email,'.$user_id,
            'phone_no' => 'required|numeric|digits:10|unique:users,phone_no,'.$user_id,
        ],[
            'first_name.regex' => 'The first name Should be letters only.',
            'last_name.regex' => 'The first name Should be letters only.'
        ]);

        User::where('id',$user_id)->update([

            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
           // 'user_status' => $request->user_status,
           // 'date_of_joining' => $request->date_of_joining,
            'address' => $request->address,
            'user_name' => $request->user_name,
            'phone_no' => $request->phone_no,
        ]);
        $notification = array(
            'message' => 'Profile updated successfully.',
            'alert-type' => 'success'
        );

        return Redirect::to('admin')->with($notification);
       // flash('Profile updated successfully.')->success();
       // return redirect()->back();
    }
    public function changePassword(){
        $user_detail = $this->adminInfo();
//        dd($user_detail->id);
        return view('admin.profile.change-password',compact('user_detail'));


    }

    public function postChangePassword(Request $request){

        $user_id = $request->admin_id;

        $this->validate($request,[
            'current_password' => 'required',
            'password' => 'required|min:6|max:20',
            'confirm_password' => 'required|min:6|max:20|same:password',
        ]);

        if (!(Hash::check($request->current_password, Auth::user()->password))) {
            // The passwords matches
            //flash('Your current password does not matches with the password you provided. Please try again.')->error();
            $notification = array(
                'message' => 'Your current password does not matches with the password you provided. Please try again.',
                'alert-type' => 'error'
            );

            return Redirect::to('change-password')->with($notification);
           // return redirect()->back();
        }else{
            User::where('id',$user_id)->update([

                'password' => bcrypt($request->password),
            ]);
        }
        $notification = array(
            'message' => 'Password updated successfully.',
            'alert-type' => 'success'
        );

        return Redirect::to('admin')->with($notification);
       // flash('Password updated successfully.')->success();
       // return redirect('admin');

    }
    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function adminInfo()
    {
        $id = User::where('user_type', 'admin')->where('user_status', 'enable')->value('id');
        if ($id == '') {
            flash('Oops! Something went wrong.')->error();
            return redirect('admin');
        }
        return $user_detail = User::find($id);
    }
}
