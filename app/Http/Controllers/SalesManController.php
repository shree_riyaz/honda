<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Redirect;
class SalesManController extends Controller
{
    public function addSalesMan(){
        return view('admin.sales-person.add');
    }

    public function postAddSalesMan(Request $request){

        $this->validate($request,[
            'first_name' => 'required|max:50|regex:/^[a-zA-Z]+$/u|string',
            'last_name' => 'required|max:50|regex:/^[a-zA-Z]+$/u|string',
            'user_status' => 'required',
            'date_of_joining' => 'required|date|date_format:Y-m-d',
            'address' => 'required',
            'user_name' => 'required|max:50|unique:users,user_name',
            'email' => 'required|email|unique:users,email',
            'phone_no' => 'required|numeric|digits:10|unique:users,phone_no',
            'password' => 'required|min:8|max:20',
            'confirm_password' => 'required|min:8|max:20|same:password',
        ],[
            'first_name.regex' => 'The first name Should be letters only.',
            'last_name.regex' => 'The last name Should be letters only.'
        ]);

        $user_details = array_merge($request->all(), ['user_type' => 'sales_person','is_deleted' => 0,'password' => bcrypt($request->password)]);
        User::create($user_details);

        $notification = array(
            'message' => 'Sales Person added successfully.',
            'alert-type' => 'success'
        );

        return Redirect::to('show/sales-man')->with($notification);
       // flash('User added successfully.')->success();
        //return redirect('show/sales-man');
    }

    public function showSalesMan(){

        $user_list = User::where('user_type','sales_person')->orderBy('id','DESC')->get();
        return view('admin.sales-person.show',compact('user_list'));
    }

    public function editSalesMan($id){

        $user_detail = User::find($id);
        return view('admin.sales-person.edit',compact('user_detail','id'));
    }
    public function updateSalesMan(Request $request){

        $user_id = $request->sales_person_id;
        if (isset($request->password)){
            $password_validation = 'required|min:8|max:20';
            $confirm_password_validation = 'required|min:8|max:20|same:password';
        }else{
            $password_validation = '';
            $confirm_password_validation ='';
        }
        $this->validate($request,[
            'first_name' => 'required|max:50|regex:/^[a-zA-Z]+$/u|string',
            'last_name' => 'required|max:50|regex:/^[a-zA-Z]+$/u|string',
            'user_status' => 'required',
            'date_of_joining' => 'required|date|date_format:Y-m-d',
            'address' => 'required',
            'user_name' => 'required|max:50|unique:users,user_name,'.$user_id,
//            'email' => 'required|email|unique:users,email,'.$user_id,
            'phone_no' => 'required|numeric|digits:10|unique:users,phone_no,'.$user_id,
            'password' => $password_validation,
            'confirm_password' => $confirm_password_validation,
        ],[
            'first_name.regex' => 'The first name Should be letters only.',
            'last_name.regex' => 'The Last name Should be letters only.'
        ]);

        User::where('id',$user_id)->update([

            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'user_status' => $request->user_status,
            'date_of_joining' => $request->date_of_joining,
            'address' => $request->address,
            'user_name' => $request->user_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'phone_no' => $request->phone_no,
        ]);

        $notification = array(
            'message' => 'Sales Person updated successfully.',
            'alert-type' => 'success'
        );

        return Redirect::to('show/sales-man')->with($notification);
       // flash('User updated successfully.')->success();
       // return redirect()->back();
    }

    public function deleteSalesMan(Request $request,$id){

        $finduser = User::find($id)->delete();

        $result =  $finduser ?   1 :  0;


        $alert = [
            'type' => $result ? 1 : 0,
            'message' => $result ? 'Record Deleted Successfully.' : 'Deletion failed.',
            'trId' => $id,
        ];
        return response()->json($alert);

    }
}
