<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Product;
class ProductController extends RestfulController
{
    public $successStatus = 200;
    public $failStatus = 501;


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function showProducts(){

        $product_lists = Product::where('status','a')->get();
//        dd($product_lists[0]->product_image);
        if (count($product_lists) > 0){
           // return response()->json(['success' => $product_lists,'status' => $this->successStatus]);
            foreach ($product_lists as $product_list){
                $product_list['product_image'] = url('product/images/'.$product_list['product_image']);
                $list_of_products[] = $product_list;
            }
//            dd($kk);
            return $this->response($list_of_products);
        }else{
            return $this->_error('No data available.');
//            return response()->json(['success' => 'No data available.','status' => $this->successStatus]);
        }
    }

}