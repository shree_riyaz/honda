<?php

namespace App\Http\Controllers\Api;

use App\FollowUp;
use App\FollowUpEnquiryStatus;
use App\FollowupFeedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class FollowUpController extends RestfulController
{
    public $successStatus = 200;
    public $failStatus = 501;

    /**
     * @param Request $request
     * @return string
     */
    public function AddFollowUp(Request $request){

        $this->validate($request,[
            'first_name' => 'required|max:50|regex:/^[a-zA-Z]+$/u|string',
            'last_name' => 'required|max:50|regex:/^[a-zA-Z]+$/u|string',
            'customer_feedback' => 'required',
            'product_id' => 'required',
            'followup_enquiry_status' => 'required',
            'enquiry_description' => 'required',
            'customer_response' => 'required',
            'is_product_sold' => 'required',
//            'date_of_communication' => 'required|date',
            'address' => 'required',
            'email' => 'required|email|unique:follow_ups,email',
            'phone_no' => 'required|numeric|digits:10|unique:users,phone_no',
        ],[
            'first_name.regex' => 'The first name Should be letters only.',
            'last_name.regex' => 'The last name Should be letters only.'
        ]);



        $user_details = array_merge($request->all(), ['sales_person_id' =>Auth::user()->id ,'user_count'=>1,'date_of_communication' => date('Y-m-d H:i:s')]);
        $is_saved = FollowUp::create($user_details);

        $followup_feedback = [
            'customer_feedback' => $request['customer_feedback'],'attempt_number' => 1,'product_id' => $request['product_id'],'sales_person_id' =>Auth::user()->id,'followup_id'=>$is_saved->id, 'followup_enquiry_status' => $request['followup_enquiry_status'],'enquiry_description' => $request['enquiry_description'],'customer_response' => $request['customer_response'],'is_product_sold' => $request['is_product_sold']
        ];

//        dd($followup_feedback);

        FollowupFeedback::create($followup_feedback);

        if ($is_saved){
            return $this->response($user_details);
           // return response()->json(['success' => 'Followup added successfully.','status' => $this->successStatus]);
        }else{
            return $this->_error('Oops! Something went wrong.');
//            return response()->json(['success' => 'Oops! Something went wrong.','status' => $this->failStatus]);

        }
    }

    public function updateFollowUp(Request $request){
        $is_followup_edit = $request->is_followup_edit;
        $followup_id = (int)$request->followup_id;
        if ($is_followup_edit == "true"){

            $this->validate($request,[
                'first_name' => 'required|max:50|regex:/^[a-zA-Z]+$/u|string',
                'last_name' => 'required|max:50|regex:/^[a-zA-Z]+$/u|string',
                'customer_feedback' => 'required',
                'product_id' => 'required',
                'followup_enquiry_status' => 'required',
                'enquiry_description' => 'required',
                'customer_response' => 'required',
                'is_product_sold' => 'required',
    //            'date_of_communication' => 'required|date',
                'address' => 'required',
    //            'email' => 'required|email|unique:follow_ups,email,'.$followup_id,
                'phone_no' => 'required|numeric|digits:10|unique:follow_ups,phone_no,'.$followup_id,
            ],[
                'first_name.regex' => 'The first name Should be letters only.',
                'last_name.regex' => 'The last name Should be letters only.'
            ]);


            $followUpdata = FollowUp::where(['id' => $followup_id])->first();
//                dd();

            if (empty($followUpdata)){
                return $this->_error('No data found.');
            }else{
                $followUpdata->first_name = $request->first_name;
                $followUpdata->last_name = $request->last_name;
                $followUpdata->followup_enquiry_status = $request->followup_enquiry_status;
                $followUpdata->phone_no = $request->phone_no;
                $followUpdata->address = $request->address;
                $followUpdata->is_product_sold = $request->is_product_sold;
                $followUpdata->enquiry_description = $request->enquiry_description;
                $followUpdata->customer_response = $request->customer_response;
                $followUpdata->product_id = $request->product_id;
                $followUpdata->sales_person_id = Auth::user()->id;
//            $followUpdata->user_count = $followUpdata->user_count + 1;
                $is_updated = $followUpdata->save();
//            $is_updated = 1;

                if ($is_updated){
                    $follow_list_details = FollowupFeedback::where('followup_id',$followup_id)->orderBy('created_at','desc')->get();

                    $followUpdataFeddback = FollowupFeedback::where(['id' => $follow_list_details[0]->id])->first();

                    $followUpdataFeddback->customer_feedback = $request['customer_feedback'];
                    $followUpdataFeddback->product_id = $request['product_id'];
                    $followUpdataFeddback->sales_person_id = Auth::user()->id;
                    $followUpdataFeddback->followup_id = $followup_id;
                    $followUpdataFeddback->followup_enquiry_status = $request['followup_enquiry_status'];
                    $followUpdataFeddback->enquiry_description = $request['enquiry_description'];
                    $followUpdataFeddback->customer_response = $request['customer_response'];
                    $followUpdataFeddback->is_product_sold = $request['is_product_sold'];
                    $followUpdataFeddback->save();

                    //  return response()->json(['success' => 'Followup updated successfully.','status' => $this->successStatus]);
                    return $this->response($followUpdata);
                }else{
                    return $this->_error('Oops! Something went wrong.');
                    //return response()->json(['error' => 'No data found to delete.','status' => $this->failStatus]);
                }
            }

            }
        else{
            $this->validate($request,[
                'first_name' => 'required|max:50|regex:/^[a-zA-Z]+$/u|string',
                'last_name' => 'required|max:50|regex:/^[a-zA-Z]+$/u|string',
                'customer_feedback' => 'required',
                'product_id' => 'required',
                'followup_enquiry_status' => 'required',
                'enquiry_description' => 'required',
                'customer_response' => 'required',
                'is_product_sold' => 'required',
                //            'date_of_communication' => 'required|date',
                'address' => 'required',
                //            'email' => 'required|email|unique:follow_ups,email,'.$followup_id,
                'phone_no' => 'required|numeric|digits:10|unique:follow_ups,phone_no,'.$followup_id,
            ],[
                'first_name.regex' => 'The first name Should be letters only.',
                'last_name.regex' => 'The last name Should be letters only.'
            ]);

            $follow_list_details = FollowupFeedback::where('followup_id',$followup_id)->get();
            $attempt_number = count($follow_list_details) + 1;

            $followUpdata = FollowUp::where(['id' => $followup_id])->first();

            if (empty($followUpdata)){
                return $this->_error('No data found.');
            }else{

                $followUpdata->first_name = $request->first_name;
                $followUpdata->last_name = $request->last_name;
                $followUpdata->followup_enquiry_status = $request->followup_enquiry_status;
                $followUpdata->phone_no = $request->phone_no;
                $followUpdata->address = $request->address;
                $followUpdata->is_product_sold = $request->is_product_sold;
                $followUpdata->enquiry_description = $request->enquiry_description;
                $followUpdata->customer_response = $request->customer_response;
                $followUpdata->product_id = $request->product_id;
                $followUpdata->sales_person_id = Auth::user()->id;

                if ($attempt_number <= 3) {
//                $followUpdata->user_count = $followUpdata->user_count + 1;
                    $followUpdata->user_count = $attempt_number;
                }
                $is_updated = $followUpdata->save();

                if ($is_updated){
                    if ($attempt_number <= 3){
                        $followup_feedback = [
                            'customer_feedback' => $request['customer_feedback'],'attempt_number' => $attempt_number,'product_id' => $request['product_id'],'sales_person_id' =>Auth::user()->id,'followup_id'=>$followup_id, 'followup_enquiry_status' => $request['followup_enquiry_status'],'enquiry_description' => $request['enquiry_description'],'customer_response' => $request['customer_response'],'is_product_sold' => $request['is_product_sold']
                        ];

                        FollowupFeedback::create($followup_feedback);
                    }
                    return $this->response($followUpdata);
                }else{
                    return $this->_error('Oops! Something went wrong.');

                }
            }
        }
    }

    public function deleteFollowUp(Request $request){

        $followup_id = $request->followup_id;
        $is_deleted = FollowUp::where('id',$followup_id)->delete();
//        dd($is_deleted);
        if($is_deleted){

            return $this->response(['Followup deleted successfully.']);
           // return response()->json(['success' => 'Followup deleted successfully.','status' => $this->successStatus]);
        }else{
            return $this->_error('Oops! Something went wrong.');
            //return response()->json(['error' => 'Oops! Something went wrong.','status' => $this->failStatus]);

        }
    }

    public function listFollowUp(Request $request){

//        dd(isset($request['status']));

        if (isset($request['status'])){
            $follow_list = FollowUp::with('products','followup_feedback')->where('sales_person_id',Auth::user()->id)->where('followup_enquiry_status',$request['status'])->orderBy('id','desc')->get();
        }else{
            $follow_list = FollowUp::with('products','followup_feedback')->where('sales_person_id',Auth::user()->id)->orderBy('id','desc')->get();
        }

        if(count($follow_list) > 0){
            return $this->response($follow_list);
//            return response()->json(['success' => $follow_list,'status' => $this->successStatus]);
        }else{
            return $this->_error('No data available.');
            //return response()->json(['error' => 'No data available.','status' => $this->failStatus]);
        }
    }

    public function searchFollowUp(Request $request){

        $search_list = FollowUp::where('first_name','LIKE',"%{$request->first_name}%")
            ->where('last_name','LIKE',"%{$request->last_name}%")
            ->where('phone_no','LIKE',"%{$request->phone_no}%")
            ->where(['sales_person_id' => Auth::user()->id])
            ->where('email','LIKE',"%{$request->email}%")->get();

        if($search_list != ''){
            return $this->response($search_list);
           // return response()->json(['success' => $search_list,'status' => $this->successStatus]);
        }else{
            return $this->_error('No data available.');
            //return response()->json(['success' => 'No data available.','status' => $this->successStatus]);
        }
    }

    public function listFollowUpEnquiryStatus(){
        $followup_enquiry_list = FollowUpEnquiryStatus::all();
        if(count($followup_enquiry_list) > 0){
            return $this->response($followup_enquiry_list);
        }else{
            return $this->_error('No data available.');
        }
    }
}
