<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Carbon;

class UserController extends RestfulController
{
    public $successStatus = 200;
    public $failStatus = 503;


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){

//        dd($request->all());
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){

            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->first_name;
            $success['status'] =  $this->successStatus;
            //return response()->json(['success' => $success]);
            return $this->response($success);
        }
        else{
           // return response()->json(['error'=>'Unauthorised'], 401);
            return $this->_error('Unauthorised');
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function showUsersList(){

        $user_lists = User::where('user_type','sales_person')->get();
        if (count($user_lists) > 0){
            return $this->response($user_lists);
           // return response()->json(['success' => $user_lists,'status' => $this->successStatus]);
        }else{
            return $this->_error('Unauthorised');
            //return response()->json(['success' => 'No data available.','status' => $this->successStatus]);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function postLogout(){

        if (Auth::check()) {
           $logout = Auth::user()->AauthAcessToken()->delete();
            return $this->response($logout);
//            return response()->json(['success' => $logout,'status' => $this->successStatus]);
        }else{
            return $this->_error('Oops! Please login.');
//            return response()->json(['error' => 'Oops! Please login.','status' => $this->failStatus]);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getServerTime(){

        $currentDatetime = Carbon::now()->toDateTimeString();
        return $this->response($currentDatetime);
       //return response()->json(['status' => 'success','date'=>$currentDatetime, 'status_code' => $this->successStatus]);
    }

}
